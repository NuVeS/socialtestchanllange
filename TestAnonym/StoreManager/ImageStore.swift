//
//  ImageStore.swift
//  TestAnonym
//
//  Created by Максуд on 31/03/2019.
//  Copyright © 2019 Максуд. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class ImageStore{
    
    //MARK: Private
    
    private lazy var imageURLs = applicationDocumentsDirectory.appendingPathComponent("images")
    
    private lazy var applicationDocumentsDirectory: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    func getImage(by path: URL) -> UIImage?{
        let imageName = path.lastPathComponent
        let imagePath = imageURLs.appendingPathComponent("/\(imageName)")
        
        guard let data = try? Data(contentsOf: imagePath) else{
            return nil
        }
        
        let image = UIImage(data: data)
        return image
    }
    
    func cache(image: Data, by path: URL){
        let imageName = path.lastPathComponent
        let imagePath = imageURLs.appendingPathComponent("/\(imageName)")
        
        try? image.write(to: imagePath)
    }
}
