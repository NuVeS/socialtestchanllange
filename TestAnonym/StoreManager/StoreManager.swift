//
//  StoreManager.swift
//  TestAnonym
//
//  Created by Максуд on 31/03/2019.
//  Copyright © 2019 Максуд. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class StoreManager{
    static let shared = StoreManager()
    
    private let imageStorage = ImageStore()
    private let cacheStorage = CacheStore()
    
    lazy var fetchedResultsController: NSFetchedResultsController<FeedItemMO> = {
        let managedContext = cacheStorage.moContext
        let fetchRequest = NSFetchRequest<FeedItemMO>(entityName: "FeedItemMO")
        let sortDescriptor = NSSortDescriptor(key: "id", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        let fetchedResultsController = NSFetchedResultsController<FeedItemMO>(fetchRequest: fetchRequest, managedObjectContext: managedContext, sectionNameKeyPath: nil, cacheName: nil)
        
        return fetchedResultsController
    }()
    
    func getImage(url: URL) -> UIImage?{
        return imageStorage.getImage(by: url)
    }
    
    func saveImage(data imageData: Data, path: URL){
        imageStorage.cache(image: imageData, by: path)
    }
    
    func saveFeedItems(items: [FeedItem]){
        cacheStorage.saveItems(contentsOf: items)
    }
}
