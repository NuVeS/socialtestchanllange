//
//  CacheStore.swift
//  TestAnonym
//
//  Created by Максуд on 31/03/2019.
//  Copyright © 2019 Максуд. All rights reserved.
//

import Foundation
import CoreData

class CacheStore{
    private lazy var applicationDocumentsDirectory: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    private lazy var cacheModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "TestAnonym", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    private lazy var cachePersistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.cacheModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("Cache.sqlite")
        do {
            try coordinator.addPersistentStore(ofType: NSInMemoryStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            let wrappedError = NSError(domain: "Failed loading main cache.", code: 9999, userInfo: nil)
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
        }
        
        return coordinator
    }()
    
    lazy var moContext: NSManagedObjectContext = {
        let coordinator = self.cachePersistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    private func saveContext () {
        if moContext.hasChanges {
            do {
                try moContext.save()
            } catch {
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func saveItem(item: FeedItem){
        let moItem = FeedItemMO(context: moContext)
        let moLikes = LikeMO(context: moContext)
        let moAttachment = AttachmentMO(context: moContext)
        let moPhoto = PhotoMO(context: moContext)
        
        moLikes.count = Int32(item.likes?.count ?? 0)
        moLikes.my = item.likes?.my ?? false
        
        moPhoto.photo_big = item.attachments?.first?.photo?.photo_big
        moPhoto.photo_medium = item.attachments?.first?.photo?.photo_medium
        moPhoto.photo_small = item.attachments?.first?.photo?.photo_small
        
        moAttachment.link = item.attachments?.first?.link
        moAttachment.photo = moPhoto
        moAttachment.likes = moLikes
        
        moItem.id = Int32(item.id ?? 0)
        moItem.text = item.text
        moItem.owner_name = item.ownerName
        moItem.owner_photo = item.ownderAvatar
        moItem.attachment = moAttachment
    }
    
    func saveItems(contentsOf items: [FeedItem]){
        for item in items{
            saveItem(item: item)
        }
        saveContext()
    }
}
