//
//  NetworkManager.swift
//  TestAnonym
//
//  Created by Максуд on 31/03/2019.
//  Copyright © 2019 Максуд. All rights reserved.
//

import Foundation

class NetworkManager {
    static let shared = NetworkManager()
    
    private let apiv1 = "http://dev.apianon.ru:3000/posts/get"
    
    func prepareHeaders() -> [String:String]{
        return ["Content-Type" : "application/json"]
    }
    
    func loadFeedItems(starting: Int, count: Int, completion: @escaping ()->Void){
        let params = ["count": count, "offset" : starting, "type" : 1]
        
        guard let url = URL(string: apiv1) else{
            completion()
            return
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        urlRequest.allHTTPHeaderFields = prepareHeaders()
        
        urlRequest.httpBody = try? JSONSerialization.data(withJSONObject: params)
        let dataTask = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if error != nil{
                completion()
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse, (200...299) ~= httpResponse.statusCode, let data = data else{
                // Error loading
                completion()
                return
            }
            
            if let feed = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any], let items = feed["data"]{
                
                if let dataDecode = try? JSONSerialization.data(withJSONObject: items),
                    let feedlist = try? JSONDecoder().decode([FeedItem].self, from: dataDecode){
                        StoreManager.shared.saveFeedItems(items: feedlist)
                }
            }

            completion()
        }
        
        dataTask.resume()
    }
}
