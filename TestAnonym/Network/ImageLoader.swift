//
//  ImageLoader.swift
//  TestAnonym
//
//  Created by Максуд on 31/03/2019.
//  Copyright © 2019 Максуд. All rights reserved.
//

import Foundation
import UIKit

class ImageLoader{
    class func loadImage(from urlString: String, completion: @escaping (UIImage?) -> Void ){
        guard let url = URL(string: urlString) else{
            return
        }
        
        guard let image = StoreManager.shared.getImage(url: url) else{
            let dataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if error != nil{
                    completion(nil)
                    return
                }
                
                if let data = data{
                    StoreManager.shared.saveImage(data: data, path: url)
                    completion(UIImage(data: data))
                }else{
                    completion(nil)
                }
            }
            
            dataTask.resume()
            return
        }
        
        completion(image)
        
    }
}
