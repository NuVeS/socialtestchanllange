//
//  FeedItem.swift
//  TestAnonym
//
//  Created by Максуд on 31/03/2019.
//  Copyright © 2019 Максуд. All rights reserved.
//

import Foundation

struct FeedItem: Decodable {
    enum CodingKeys: String, CodingKey {
        case count
        case my
        case id
        case text
        case owner_name
        case owner_photo
        case likes
        case attachments
        case photo
        case photo_big
        case photo_medium
        case photo_small
        case type
        case link
    }
    
    struct Likes: Decodable {
        var count: Int?
        var my: Bool?
        
        init(from decoder: Decoder) throws {
            let container = try? decoder.container(keyedBy: CodingKeys.self)
            
            count = try! container?.decode(Int.self, forKey: .count)
            my = try! container?.decode(Int.self, forKey: .my) == 0 ? false : true
        
            
        }
        
        init?(withObject moObject: LikeMO?){
            guard let moObject = moObject else{
                return nil
            }
            count = Int(moObject.count)
            my = moObject.my
        }
    }
    
    struct Photo: Decodable {
        var photo_big: String?
        var photo_medium: String?
        var photo_small: String?
        
        init(from decoder: Decoder) throws {
            let container = try? decoder.container(keyedBy: CodingKeys.self)
            
            photo_big = try? container?.decode(String.self, forKey: .photo_big)
            photo_medium = try? container?.decode(String.self, forKey: .photo_medium)
            photo_small = try? container?.decode(String.self, forKey: .photo_small)
        }
        
        init?(withObject moObject: PhotoMO?){
            guard let moObject = moObject else{
                return nil
            }
            
            photo_big = moObject.photo_big
            photo_medium = moObject.photo_medium
            photo_small = moObject.photo_small
        }
        
    }
    
    struct Attachment: Decodable {
        var link: String?
        var photo: Photo?
        
        init(from decoder: Decoder) {
            let container = try? decoder.container(keyedBy: CodingKeys.self)
            
            link = try? container?.decode(String.self, forKey: .link)
            photo = try? container?.decode(Photo.self, forKey: .photo)
        }
        
        init?(withObject moObject: AttachmentMO?){
            guard let moObject = moObject else{
                return nil
            }
            
            link = moObject.link
            photo = Photo(withObject: moObject.photo)
            
        }
    }
    
    var id: Int?
    var text: String?
    var ownerName: String?
    var ownderAvatar: String?
    var likes: Likes?
    var attachments: [Attachment]?
    
    init(from decoder: Decoder) throws {
        let container = try? decoder.container(keyedBy: CodingKeys.self)
        
        id = try? container?.decode(Int.self, forKey: .id)
        text = try? container?.decode(String.self, forKey: .text)
        ownerName = try? container?.decode(String.self, forKey: .owner_name)
        ownderAvatar = try? container?.decode(String.self, forKey: .owner_photo)
        likes = try? container?.decode(Likes.self, forKey: .likes)
        attachments = try? container?.decode([Attachment].self, forKey: .attachments)
    }
    
    init?(withObject moObject: FeedItemMO?){
        guard let moObject = moObject else{
            return nil
        }
        
        id = Int(moObject.id)
        text = moObject.text
        ownerName = moObject.owner_name
        ownderAvatar = moObject.owner_photo
        
        likes = Likes(withObject: moObject.attachment?.likes)
        
        if let attachment = Attachment(withObject: moObject.attachment){
            attachments = [attachment]
        }else{
            attachments = []
        }
    }
    
}
