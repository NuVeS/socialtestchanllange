//
//  ViewController.swift
//  TestAnonym
//
//  Created by Максуд on 31/03/2019.
//  Copyright © 2019 Максуд. All rights reserved.
//

import UIKit
import CoreData


protocol FeedControlling {
    var offset: Int { get set }
    var defaultCount: Int { get }
    func loadMoreItems()
}

class FeedListController: UIViewController, FeedControlling {

    var offset: Int = 0
    let defaultCount: Int = 10
    
    var feedCoordinator: FeedCoordinator = FeedCoordinator()
    @IBOutlet weak var feedTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureFeedCoordinator()
        loadItems()
    }
    
    private func configureFeedCoordinator(){
        feedCoordinator.feedTableView = feedTableView
        feedCoordinator.delegate = self
        feedTableView.dataSource = feedCoordinator
        feedTableView.prefetchDataSource = feedCoordinator
        
        feedCoordinator.configureFetchController()
    }
    
    func loadItems(){
        NetworkManager.shared.loadFeedItems(starting: offset, count: defaultCount) {}
    }
    
    func loadMoreItems(){
        offset += 10
        loadItems()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let id = segue.identifier else {
            return
        }
        if id == "DetailSegue"{
            let indexPath = feedTableView.indexPathForSelectedRow
            let item = FeedItem(withObject: StoreManager.shared.fetchedResultsController.object(at: indexPath!))
            
            let vc = segue.destination as? FeedItemController
            vc?.item = item
        }
    }
}

