//
//  FeedCoordinator.swift
//  TestAnonym
//
//  Created by Максуд on 31/03/2019.
//  Copyright © 2019 Максуд. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class FeedCoordinator: NSObject{
    var feedTableView: UITableView?
    var delegate: FeedControlling?
    
    func configureFetchController() {
        StoreManager.shared.fetchedResultsController.delegate = self
        try? StoreManager.shared.fetchedResultsController.performFetch()
    }
}

extension FeedCoordinator: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return StoreManager.shared.fetchedResultsController.fetchedObjects?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Item") as? FeedItemCell
        
        let item = StoreManager.shared.fetchedResultsController.object(at: indexPath)
        
        if let cell = cell{
            configureCell(cell: cell, withItem: item)
        }
        
        return cell!
    }
    
    func configureCell(cell: FeedItemCell, withItem item: FeedItemMO){
        
        cell.itemOwner.text = item.owner_name
        
        //small image is used because not all attachments have bigger sizes
        if let photoUrl = item.attachment?.photo?.photo_small{
            cell.refreshActivity.startAnimating()
            cell.itemImage.image = nil
            ImageLoader.loadImage(from: photoUrl) {(image) in
                DispatchQueue.main.async {
                    cell.itemImage.image = image
                    cell.refreshActivity.stopAnimating()
                }
            }
        }
    }
    
}

extension FeedCoordinator: UITableViewDataSourcePrefetching{
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        if let row = indexPaths.last?.row, let count = StoreManager.shared.fetchedResultsController.fetchedObjects?.count{
            if row >= count - 1{
                delegate?.loadMoreItems()
            }
        }
    }
}

extension FeedCoordinator: NSFetchedResultsControllerDelegate{
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        feedTableView?.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                feedTableView?.insertRows(at: [indexPath], with: .fade)
            }
        case .update:
            if let indexPath = indexPath, let cell = feedTableView?.cellForRow(at: indexPath) as? FeedItemCell{
                let item = StoreManager.shared.fetchedResultsController.object(at: indexPath)
                configureCell(cell: cell, withItem: item)
            }
        default:
            break
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        feedTableView?.endUpdates()
    }
}
