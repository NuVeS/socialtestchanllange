//
//  FeedItemCell.swift
//  TestAnonym
//
//  Created by Максуд on 31/03/2019.
//  Copyright © 2019 Максуд. All rights reserved.
//

import UIKit

class FeedItemCell: UITableViewCell {

    @IBOutlet weak var refreshActivity: UIActivityIndicatorView!{
        didSet{
            refreshActivity.hidesWhenStopped = true
        }
    }
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemOwner: UILabel!
    
    override func prepareForReuse() {
        itemImage.image = nil
        itemOwner.text = nil
    }
}
