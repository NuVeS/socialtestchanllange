//
//  FeedItemController.swift
//  TestAnonym
//
//  Created by Максуд on 31/03/2019.
//  Copyright © 2019 Максуд. All rights reserved.
//

import UIKit

class FeedItemController: UIViewController {

    @IBOutlet private  weak var feedTitle: UILabel!
    @IBOutlet private weak var likesLabel: UILabel!
    @IBOutlet private weak var feedImage: UIImageView!
    
    var item: FeedItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
    }
    
    func configureView(){
        self.title = item?.ownerName
        feedTitle.text = item?.text
        likesLabel.text = "Нравится: \(item?.likes?.count ?? 0)"
        if let url = item?.attachments?.first?.photo?.photo_big{
            loadImage(url: url)
        }
        
    }
    
    private func loadImage(url: String){
        ImageLoader.loadImage(from: url) { (image) in
            DispatchQueue.main.async { [unowned self] in
                self.feedImage.image = image
            }
        }
    }
}
