//
//  Attachment+CoreDataProperties.swift
//  TestAnonym
//
//  Created by Максуд on 31/03/2019.
//  Copyright © 2019 Максуд. All rights reserved.
//
//

import Foundation
import CoreData


extension AttachmentMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AttachmentMO> {
        return NSFetchRequest<AttachmentMO>(entityName: "Attachment")
    }

    @NSManaged public var link: String?
    @NSManaged public var photo: PhotoMO?
    @NSManaged public var likes: LikeMO?

}
