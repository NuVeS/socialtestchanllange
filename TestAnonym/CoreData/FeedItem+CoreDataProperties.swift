//
//  FeedItem+CoreDataProperties.swift
//  TestAnonym
//
//  Created by Максуд on 31/03/2019.
//  Copyright © 2019 Максуд. All rights reserved.
//
//

import Foundation
import CoreData


extension FeedItemMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FeedItemMO> {
        return NSFetchRequest<FeedItemMO>(entityName: "FeedItem")
    }

    @NSManaged public var id: Int32
    @NSManaged public var text: String?
    @NSManaged public var owner_name: String?
    @NSManaged public var owner_photo: String?
    @NSManaged public var attachment: AttachmentMO?

}
