//
//  Like+CoreDataProperties.swift
//  TestAnonym
//
//  Created by Максуд on 31/03/2019.
//  Copyright © 2019 Максуд. All rights reserved.
//
//

import Foundation
import CoreData


extension LikeMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<LikeMO> {
        return NSFetchRequest<LikeMO>(entityName: "Like")
    }

    @NSManaged public var count: Int32
    @NSManaged public var my: Bool

}
