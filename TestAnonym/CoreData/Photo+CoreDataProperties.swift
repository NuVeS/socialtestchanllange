//
//  Photo+CoreDataProperties.swift
//  TestAnonym
//
//  Created by Максуд on 31/03/2019.
//  Copyright © 2019 Максуд. All rights reserved.
//
//

import Foundation
import CoreData


extension PhotoMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PhotoMO> {
        return NSFetchRequest<PhotoMO>(entityName: "Photo")
    }

    @NSManaged public var photo_big: String?
    @NSManaged public var photo_medium: String?
    @NSManaged public var photo_small: String?

}
