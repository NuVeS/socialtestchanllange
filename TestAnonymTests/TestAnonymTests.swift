//
//  TestAnonymTests.swift
//  TestAnonymTests
//
//  Created by Максуд on 31/03/2019.
//  Copyright © 2019 Максуд. All rights reserved.
//

import XCTest
@testable import TestAnonym

class TestAnonymTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testSavingImage() {
        
        let pathName = "Testing/1"
        let image = UIImage(named: "TestImage1")
        
        StoreManager.shared.saveImage(data: image!.pngData()!, path: URL(string:pathName)!)
        
        let imageRestored = StoreManager.shared.getImage(url: URL(string:pathName)!)
        XCTAssert(imageRestored != nil)
    }

    func testRestoringImages(){
        let pathName1 = "Testing/1"
        let image1 = UIImage(named: "TestImage1")
        
        let pathName2 = "Testing/2"
        let image2 = UIImage(named: "TestImage2.jpg")
        
        
        StoreManager.shared.saveImage(data: image1!.pngData()!, path: URL(string:pathName1)!)
        StoreManager.shared.saveImage(data: image2!.pngData()!, path: URL(string:pathName2)!)
        
        let imageRestored = StoreManager.shared.getImage(url: URL(string:pathName1)!)
        XCTAssert(imageRestored?.isEqual(image1) == true)
    }
    
    func testSavingFeedItems(){
        let url = Bundle(for: self.classForCoder).url(forResource: "TestFeed", withExtension: "")
        try? StoreManager.shared.fetchedResultsController.performFetch()
        
        let data = try! Data(contentsOf: url!)
        
        let feed = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any]
        let items = feed["data"]!
        let dataDecode = try! JSONSerialization.data(withJSONObject: items)
        let feedlist = try! JSONDecoder().decode([FeedItem].self, from: dataDecode)
        StoreManager.shared.saveFeedItems(items: feedlist)
        
        let count = StoreManager.shared.fetchedResultsController.fetchedObjects?.count
        XCTAssert(count == feedlist.count)
    }
    
}
